from tap_pipedrive.stream import PipedriveStream


class OrganizationRelationshipsStream(PipedriveStream):
    endpoint = 'organizationRelationships'
    schema = 'organization_relationships'
    state_field = 'update_time'
    key_properties = ['id']
    replication_method = 'INCREMENTAL'